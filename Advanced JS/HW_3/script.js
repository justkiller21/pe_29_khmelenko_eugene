class Eployee {
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    get name() {
        return this._name
    }

    set name(newName) {
        this._name = newName
    }

    get age() {
        return this._age
    }

    set age(newAge) {
        this._age = newAge
    }

    get salary() {
        return this._salary
    }

    set salary(newSalary) {
        this._salary = newSalary
    }
}

class Programmer extends Eployee{
    constructor(name,age,salary, lang) {
        super(name,age,salary);
        this._lang = lang
    }
    get lang() {
        return this._lang
    }

    set lang(newlang) {
        this._lang = newlang
    }
    get salary() {
        return this._salary
    }
    set salary(newSalary){
        this._salary = newSalary*3
    }

}

const prog1 =  new Programmer("Joseph Jostar",50,1000,"JS")
const prog2 =  new Programmer("Jonathan Jostar",20,700,"C++")
const prog3 =  new Programmer("Jotaro Kudjo",19,1500,"Java")
    prog1.salary = 500
console.log(prog1);
console.log(prog2);
console.log(prog3);