const filmUrl = "https://ajax.test-danit.com/api/swapi/films"


fetch(filmUrl)
    .then(res => res.json())
    .then(films => {
        const filmsContainer = document.createElement('ul');
        document.body.insertAdjacentElement('afterbegin', filmsContainer)
        films.forEach(film => {
        const filmsItem = document.createElement('li');
        const filmCharacters = document.createElement('ul');
            const {episodeId, name, openingCrawl, characters} = film;
            filmsItem.insertAdjacentHTML('afterbegin', `
            <h1>Episode №${episodeId}: ${name} </h1>
            <p>${openingCrawl} </p>
            `);

            const request = characters.map(character => fetch(character));
            Promise.all(request)
                .then(res => Promise.all(res.map(r => r.json())))
                .then(characters => {
                    characters.forEach(character => {
                        const {name} = character;
                        filmCharacters.insertAdjacentHTML('beforeend', `<li>${name}</li>`);
                    });
                });
            filmsItem.append(filmCharacters);
            filmsContainer.append(filmsItem);
        });
    });

c