class SearchIP {
    constructor() {
        this.elements = {
            container: document.createElement('div'),
            btn: document.createElement("button")
        }
    }

    URL = 'https://api.ipify.org/?format=json'


    async getUserAddress() {
        const {container} = this.elements
        let IP = await fetch(this.URL)
            .then(res => res.json())
        let userInfo = await fetch(`http://ip-api.com/json/${IP.ip}?fields=continent,country,regionName,city,district`)
            .then(res => res.json())
        container.insertAdjacentHTML('afterbegin', `
            <h1>I found you</h1>
            <p>${userInfo.continent}</p>
            <p>${userInfo.country}</p>
            <p>${userInfo.regionName}</p>
            <p>${userInfo.city}</p>
            <p>${userInfo.district}</p>`)
    }

    async render() {
        const {container, btn} = this.elements
        btn.textContent = `Вычислить по IP`
        await btn.addEventListener('click', () => this.getUserAddress())

        container.append(btn)
        document.body.append(container)
    }

}

let searchUser = new SearchIP
searchUser.render()

