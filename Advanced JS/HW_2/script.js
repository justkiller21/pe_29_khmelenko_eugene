const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let checkedListElements = books.map(({author, name, price}) => {
    try {
        if (author && name && price) {
            return `<li>Author: ${author}</li> 
                    <li>Name: ${name}</li> 
                    <li>Price: ${price}</li></br>`;
        } else if (!author) {
            throw new TypeError('No author');
        } else if (!name) {
            throw new TypeError('No name');
        } else if (!price) {
            throw new TypeError('No price');
        }
    } catch (error) {
        console.error(`Error: ${error.message}`);
    }
});

function arrayToList(array, parent = document.body) {
    parent.insertAdjacentHTML('beforeend', `<ul>${array.join(' ')}</ul>`);
}

arrayToList(checkedListElements, document.querySelector('#root'))
