const gulp = require("gulp"),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

const paths = {
    src: {
        scss: "./src/scss/**/*.scss",
        js: "./src/js/*.js",
        img: "./src/img/**/*"
    },
    build: {
        css: "./dist/css/",
        js: "./dist/js/",
        img: "./dist/img/",
        self: "./dist/"
    }
};

const uglifyJS = () => (
    gulp.src(paths.src.js)
        .pipe(uglify())
        .pipe(gulp.dest("./src/js/"))
)

const buildJS = () => (
    gulp.src(paths.src.js)
        .pipe(concat('script.min.js'))
        .pipe(gulp.dest(paths.build.js))
        .pipe(browserSync.stream())
);

const buildCSS = () => (
    gulp.src(paths.src.scss)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest(paths.build.css))
        .pipe(browserSync.stream())
);

const buildIMG = () => (
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.build.img))
        .pipe(browserSync.stream())
);

const cleanBuild = () => (
    gulp.src(paths.build.self, {allowEmpty: true})
        .pipe(clean())
);


const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch(paths.src.scss, buildCSS).on("change", browserSync.reload);
    gulp.watch(paths.src.js, buildJS).on("change", browserSync.reload);
    gulp.watch(paths.src.img, buildIMG).on("change", browserSync.reload);
};

const build = gulp.series(
    cleanBuild,
    buildCSS,
    uglifyJS,
    buildJS,
    buildIMG
);


gulp.task("clean", cleanBuild);
gulp.task("buildCSS", buildCSS);
gulp.task("buildJS", buildJS);
gulp.task('uglifyJS', uglifyJS);
gulp.task("build", build);
gulp.task("dev", gulp.series(watcher));
