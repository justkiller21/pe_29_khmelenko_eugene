Ответы на вопросы
=================
>1.Опишите своими словами разницу между функциями setTimeout() и setInterval().

SetTimeout() выполняет заданную первым аргументом функцию, через количество миллисекунд заданными вторым аргументом. setInterval() выполняет функцию каждый раз спустя промежуток заданного времени.
>2.Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?

Она сработает после выполнения всего остального кода.

>3.Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?

Поскольку она все равно будет выполняться и занимать память, поэтому надо функцию отключать