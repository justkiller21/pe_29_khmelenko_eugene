let images = document.querySelector(".images-wrapper").children
let count = 0
function showImages() {
    timerId = setInterval(() => {
            if (count < images.length) {
                images[count].classList.toggle("image-to-show")
                count++
            }
            if (count >= images.length) {
                count = images.length % count
                console.log(count);
            }
    }, 3000)
    return timerId
}
showImages()
const stopButton = document.querySelector('.stop')
const continueButton = document.querySelector('.continue')

continueButton.addEventListener('click', () => {
    showImages()
})
stopButton.addEventListener('click', () => {
    clearInterval(timerId)
})