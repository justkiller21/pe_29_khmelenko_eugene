function arrayToList(array, parent = document.body) {
    return parent.insertAdjacentHTML("afterbegin", `<ul>${array.map((element) => `<li>${(element)}</li>`).join('')}</ul>`)
}
arrayToList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])