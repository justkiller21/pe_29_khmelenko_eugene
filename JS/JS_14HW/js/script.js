$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault()
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500)
})

$(document).ready(function () {
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= window.innerHeight) {
            $('.scroll-button').removeClass('hide')
        }
    })
    $('.scroll-button').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500)
    })
})

$(document).ready(function () {
    $(".section-hide").on('click', function () {
        $(".top-rated").slideToggle("slow");
    });
});

