document.body.addEventListener('keydown', (e) => {
        const buttonsList = document.querySelector('.btn-wrapper').children
        for (let i = 0; i < buttonsList.length; i++) {
            if (buttonsList[i].dataset.id === e.key.toLowerCase()) {
                buttonsList[i].classList.add('btn-blue')
            } else {
                buttonsList[i].classList.remove('btn-blue')
            }
        }
    }
)
