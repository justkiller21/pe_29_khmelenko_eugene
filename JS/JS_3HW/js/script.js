let num1 = +prompt("Enter your first number")
let num2 = +prompt("Enter your second number")
let oper = prompt("Enter your operation", "+, -, *, /")

function math(num1, num2, oper) {
    switch (oper) {
        case("+"):
            return num1 + num2
        case("-"):
            return num1 - num2
        case("*"):
            return num1 * num2
        case("/"):
            return num1 / num2
    }
}

console.log(math(num1, num2, oper));
