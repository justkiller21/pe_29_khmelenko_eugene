document.addEventListener('click', (e) => {
        e.preventDefault()
        const textsList = document.querySelector(".tabs-content").children
        if (e.target.classList.contains('tabs-title')) {
            e.target.classList.toggle('active')
        }
        for (let i = 0; i < textsList.length; i++) {
            if (e.target.dataset.name === document.querySelector(".tabs-content").children[i].dataset.name) {
                document.querySelector(".tabs-content").children[i].classList.toggle('tabs-text-default')
            }
        }

    }
)

