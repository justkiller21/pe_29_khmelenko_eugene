let yourPassword = document.getElementById('your-password')
let confirmPassword = document.getElementById('confirm-password')
document.body.addEventListener('click', (e) => {
    if (e.target.classList.contains("fa-eye-slash")) {
        e.target.classList.replace("fa-eye-slash", 'fa-eye')
        e.target.previousElementSibling.type = "password";

    } else if (e.target.classList.contains("fa-eye")) {
        e.target.classList.replace("fa-eye", 'fa-eye-slash')
        e.target.previousElementSibling.type = "text";
    }
})

let submitButton = document.querySelector('.btn')
submitButton.insertAdjacentHTML('beforebegin', `<span class= "wrong-password" >Нужно ввести одинаковые значения</span>`)
submitButton.addEventListener('click', () => {
    if (yourPassword.value === confirmPassword.value) {
        document.querySelector(".wrong-password").style.color = "white"
        alert("You are welcome")
    } else {
        document.querySelector(".wrong-password").style.color = "red"
    }
})