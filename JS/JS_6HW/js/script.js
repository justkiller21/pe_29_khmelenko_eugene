function filterBy(array, userDataType) {
    return array.filter(item => typeof item !== userDataType)
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
