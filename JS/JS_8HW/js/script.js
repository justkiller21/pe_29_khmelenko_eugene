const elements = [
    {
        ID: 'validateInput',
        tag: 'input',
        class: 'input',
    },
    {
        ID: 'price',
        tag: 'span',
        class: 'price',
        textContent: 'Price'

    }
]

const createHTMLElement = (elementID, tagName, className, textContent = '', parent = document.body) => {
    let itemHTML = document.createElement(`${tagName}`)
    itemHTML.classList.add(className)
    itemHTML.textContent = `${textContent}`
    itemHTML.id = `${elementID}`
    if (parent !== document.body)
        parent = document.querySelector(`#${parent}`)
    parent.prepend(itemHTML)
}

createHTMLElement('container', 'div', 'wrapper')
elements.forEach((element) => createHTMLElement(element.ID, element.tag, element.class, element.textContent, 'container'))
createHTMLElement('error', 'span', 'error', `Please enter correct price`, 'container')
createHTMLElement('pricesContainerName', 'h1', 'prices-container-name', `Prices`)
createHTMLElement('pricesContainer', 'div', 'prices-container')


let input = document.getElementById('validateInput')
let error = document.getElementById('error')
input.addEventListener('focus', () => input.classList.add("input:focus"))
input.addEventListener('blur', () => {
    input.classList.remove("input:focus")
    if (input.value <= 0 || isNaN(input.value)) {
        error.style.visibility = 'visible'
        input.style.backgroundColor = 'red'
    } else {
        createHTMLElement('pricesItem', 'div', 'prices-item', '', 'pricesContainer')
        createHTMLElement('close', 'span', 'close', `X`, 'pricesItem')
        createHTMLElement('result', 'span', 'result', `Текущая цена: ${input.value}`, 'pricesItem')
        input.style.backgroundColor = 'green'
        error.style.visibility = 'hidden'
    }
})
document.addEventListener('click', (e) => {
    let close = document.getElementById('close')
    let result = document.getElementById('result')
    let containerItem = document.getElementById('pricesItem')
    if (e.target.classList.contains('close')) {
        close.remove()
        result.remove()
        containerItem.remove()
        input.value = ''
        input.style.backgroundColor = 'white'
    }
})