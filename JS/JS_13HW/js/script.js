let currentTheme
let startTheme = localStorage.getItem('startTheme')
if (startTheme === "light") {
    currentTheme = 'css/styles_light.css'
} else if (startTheme === "dark") {
    currentTheme = 'css/styles_dark.css'
}
document.head.children[4].href = currentTheme

let changeThemeButton = document.querySelector('.btn-change-theme')
changeThemeButton.addEventListener('click', () => {
    if (currentTheme === 'css/styles_light.css') {
        currentTheme = 'css/styles_dark.css'
        localStorage.setItem('startTheme', 'dark')
    } else if (currentTheme === 'css/styles_dark.css') {
        currentTheme = 'css/styles_light.css'
        localStorage.setItem('startTheme', 'light')
    }
    document.head.children[4].href = currentTheme
})

