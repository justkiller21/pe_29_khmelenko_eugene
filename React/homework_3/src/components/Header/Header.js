import React from 'react';
import PropTypes from 'prop-types';
import './Header.scss'


const Header = ({title = 'Default title'}) => {
    return (
        <header className='header'>
            <h1>{title}</h1>
        </header>
    );
};

export default Header;

Header.propTypes = {
    title: PropTypes.string
}