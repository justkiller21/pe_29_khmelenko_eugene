import React from 'react';
import {NavLink} from "react-router-dom";
import './NavMenu.scss'

const NavMenu = () => {
    return (
        <ul className='nav-menu__list'>
            <li className='nav-menu__item'><NavLink exact to='/products' activeClassName='selected'>Products</NavLink></li>
            <li className='nav-menu__item'><NavLink exact to='/cart' activeClassName='selected'>Cart</NavLink></li>
            <li className='nav-menu__item'><NavLink exact to='/favorites' activeClassName='selected'>Favorites</NavLink></li>
        </ul>
    );
};

export default NavMenu;