import React from 'react';
import PropTypes from 'prop-types';
import * as allIcons from "../../icons/index";

const Icon =({ type, color, className, ...rest})=> {

        const Icon = allIcons[type];

        if (!Icon) {
            return null
        }

        return (
            <div className={className} {...rest}>
                {Icon(color)}
            </div>
        );
}

Icon.defaultProps = {
    color: '#000',
    className: 'svg-wrapper',
};

Icon.propTypes = {
    type: PropTypes.string.isRequired,
    color: PropTypes.string,
    className: PropTypes.string
};

export default Icon;