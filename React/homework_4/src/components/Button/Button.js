import React from 'react';
import './Button.scss'
import PropTypes from 'prop-types';

const Button = ({classes, text, handleClick}) => {
    return (
        <button className={classes} onClick={handleClick}>
            {text}
        </button>
    );
}

Button.defaultProps = {
    classes: 'btn',
    text: '',
    handleClick: () => {
    }
};

Button.propTypes = {
    classes: PropTypes.string,
    text: PropTypes.node,
    handleClick: PropTypes.func
};


export default Button;