import React from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import './Product.scss'
import Icon from "../Icon/Icon";
import {ModalAddToCart, ModalDeleteFromCart} from "../Modal/Modal_config";


const Product = ({
                     product,
                     isFavorite,
                     openModal,
                     closeModal,
                     addToCart,
                     addToCartBool = true,
                     addToFavorite,
                     addToFavoriteBool = true,
                     deleteFromCart,
                     deleteFromCartBool = false
                 }) => {
    const star = <Icon type='star' color={isFavorite ? '#ffc107' : '#e3db97'}/>

    return (
        <div className='product__card'>
            <div className='product__header'>
                {deleteFromCartBool && <Button classes='btn btn__close'
                                               handleClick={() => openModal(<ModalDeleteFromCart
                                                   deleteFromCart={deleteFromCart}
                                                   closeModal={closeModal}
                                                   code={product.code}/>)}/>}
                {addToFavoriteBool && <Button classes='btn btn__star' text={star} handleClick={() => {
                    addToFavorite(product.code)
                }}/>}
            </div>
            <div className='product__img-wrapper'>
                <img src={product.image} alt="name" className='product__img'/>
            </div>
            <h4 className='product__title'>{product.name}</h4>
            <p className='product__text'>Color: {product.color.join(', ')}</p>
            <p className='product__text'>Price: {product.price}</p>
            <div className='product__btn-wrapper'>
                {addToCartBool && <Button classes='btn btn__add-cart' text='Add to cart'
                                          handleClick={() => openModal(<ModalAddToCart addToCart={addToCart}
                                                                                       closeModal={closeModal}
                                                                                       product={product}/>)}/>}
            </div>
        </div>
    );
}

Product.propTypes = {
    product: PropTypes.shape({
        code: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        image: PropTypes.string,
        color: PropTypes.array
    }).isRequired,
    isFavorite: PropTypes.bool.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,

}


export default Product;