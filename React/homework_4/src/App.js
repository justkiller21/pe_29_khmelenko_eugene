import React, {useEffect, useState} from "react";
import './App.scss'
import axios from "axios";
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import NavMenu from "./components/NavMenu/NavMenu";


const App = () => {
    const [modal, setModal] = useState(null)
    const [products, setProducts] = useState([])
    const openModal = (content) => {
        setModal(content)
    }
    const closeModal = () => {
        setModal(null)
    }

    const addToCart = (product) => {
        const cart = JSON.parse(localStorage.getItem('cart')) || []
        if (cart) {
            cart.push(product)
            localStorage.setItem('cart', JSON.stringify(cart))
        } else {
            localStorage.setItem('cart', JSON.stringify(product))
        }
        closeModal()
    }
    const deleteFromCart = (code) => {
        const cart = JSON.parse(localStorage.getItem('cart'))
        let newCart = cart.filter(productToDelete => productToDelete.code !== code)
        localStorage.setItem('cart', JSON.stringify(newCart))
        closeModal()
    }
    const addToFavorite = (code) => {
        let favorites = []
        if (products.find(product => product.code === code)) {
            favorites = products.map(product => product.code === code ? {
                    ...product,
                    isFavorite: !product.isFavorite
                } : product
            )
            setProducts(favorites)
        }
        localStorage.setItem('favorites', JSON.stringify(favorites.filter(product => product.isFavorite ? product.code : '')))

    }

    useEffect(() => {
        axios('/items.json')
            .then(res => {
                const products = res.data.map(product => ({...product, isFavorite: false}));
                const favorites = JSON.parse(localStorage.getItem('favorites'))
                if (favorites) {
                    const updatedProducts = products.map(product => {
                        const favoriteProduct = favorites.find(favProduct => product.code === favProduct.code);
                        return {...product, isFavorite: favoriteProduct ? true : product.isFavorite}
                    })
                    setProducts(updatedProducts)
                } else {
                    setProducts(products)
                }
            })
    }, [])


    return (
        <div className='app'>
            <Header title={'Products'}/>
            <NavMenu/>
            <AppRoutes products={products}
                       addToCart={addToCart}
                       addToFavorite={addToFavorite}
                       deleteFromCart={deleteFromCart}
                       openModal={openModal}
                       closeModal={closeModal}/>
            {modal}
        </div>
    );

}

export default App;




