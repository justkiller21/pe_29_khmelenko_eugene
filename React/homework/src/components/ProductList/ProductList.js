import React, {Component} from 'react';
import Product from "../Product/Product";
import './ProductList.scss'
import PropTypes from 'prop-types';


class ProductList extends Component {
    render() {
        const {products, openModal, closeModal, addToCart, addToFavorite} = this.props

        const productCards = products.map(product => <Product key={product.code} product={product} openModal={openModal}
                                                        closeModal={closeModal} addToCart={addToCart}
                                                        addToFavorite={addToFavorite} isFavorite={product.isFavorite}/>)
        return (
            <div className='product-list'>
                {productCards}
            </div>
        );
    }
}
ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        image: PropTypes.node.isRequired,
        code: PropTypes.number.isRequired,
        color: PropTypes.array.isRequired
    })).isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}
export default ProductList;