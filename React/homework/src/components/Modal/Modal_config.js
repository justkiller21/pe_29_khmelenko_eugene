import Modal from "./Modal";
import Button from "../Button/Button";
import {Component} from "react";
import PropTypes from 'prop-types';


export class ModalAddToCart extends Component {
    render() {
        const {closeModal, addToCart} = this.props
        return (
            <Modal
                header='Do you want this Pokemon in your cart?'
                closeButton={false}
                modalType='modal__container--success'
                closeModal={closeModal}
                desc=''
                actions={<div>
                    <Button classes='btn btn__control' text='Yes'
                            handleClick={addToCart}/>
                    <Button classes='btn btn__control' text='No'
                            handleClick={closeModal}/>
                </div>}
            />
        );
    }
}
ModalAddToCart.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired
}
