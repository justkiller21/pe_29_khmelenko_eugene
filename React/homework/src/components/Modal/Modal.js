import React, {Component} from 'react';
import './Modal.scss'
import Button from "../Button/Button";
import PropTypes from "prop-types";

class Modal extends Component {
    render() {
        const {header, desc, closeModal, modalType, closeButton, actions} = this.props;

        return (
            <div className='modal' onClick={e => (e.currentTarget === e.target) && closeModal}>
                <div className={modalType}>
                    <div className='modal__header'>
                        <span className='modal__header-title'>{header}</span>
                        {closeButton && <Button classes='btn btn__close' handleClick={closeModal}/>}
                    </div>
                    <div className="modal__content">
                        <p className='modal__content-text'>{desc}</p>
                    </div>
                    <div className='modal__control'>
                        {actions}
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;

Modal.defaultProps = {
    modalClassName: 'modal',
    headerClassName: 'modal__header',
    header: 'HeaderPlaceholder',
    closeButton: true,
    desc: 'DescPlaceholder'
};

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    desc: PropTypes.string,
    closeModal: PropTypes.func.isRequired,
    modalType: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    actions: PropTypes.node.isRequired
}